# Hudu Documentation

## Setup

Change the variables `$HuduAPIKey` and `$HuduBaseDomain` to what your information is

The other two variables you may need to change are `$singleFile` and `$outputFolder`

## What this does

This script grabs every article from Hudu and sorts it by company and then by folder.

This will also grab everything from the global KB as well.
