# bring in the modules that we need

Import-Module HuduAPI

# settings that needs to be custom for each Hudu instance

# Get a Hudu API Key
$HuduAPIKey =  "changemetosomething"
# Set the base domain of your Hudu instance without a trailing /
$HuduBaseDomain = "docs.example.com"

# this can either be $true or $false
# if $true, will output a large file for printing
$singleFile = $true
$outputFolder = ".\files\Hudu"

# settings that should work for everyone, but can be modified
# css needs to have the other callouts added to it

$htmlHeader = @"
<head>
<style>
.magic-dash {
    grid-column: span 4;
}

#folder {
    text-align: center;
    padding: 425px 0;
    border: 3px solid green;
}

#company {
    text-align: center;
    padding: 425px 0;
    border: 3px solid blue;
}

code {
    font-family: Monaco, monospace;
    font-size: 12px;
    line-height: 100%;
    background-color: #bdbdbd;
    padding: 0.1em;
    letter-spacing: -0.05em;
    word-break: normal;
    border-radius: 5px;
}

pre code {
    border: none;
    background: none;
    font-size: 12px;
    line-height: 1em;
    letter-spacing: normal;
    word-break: break-all;
}

pre {
    border: 1px solid black;
    padding: 0.2em;
}

.flex-container {
    display: flex;
    flex-flow: row wrap;
}

.flex-container div {
    background-color: #f1f1f1;
    width: 45%;
    margin: 10px;
    line-height: 45px;
    font-size: 15px;
    padding-left: 10px;
}

.callout {
    padding: 1rem;
    position: relative;
    border: 2px transparent solid;
    padding-left: calc(1.2em + 1rem + 3px);
}

.callout-info {
    border-left-color: #0288d1;
    color: #01466c;
    background-color: #d3efff;
}

.callout-success {
    border-left-color: #0f7d15;
    background-color: #eafdeb;
    color: #063409;
}

.callout-warning {
    border-left-color: #cf4d03;
    background-color: #fee3d3;
    color: #6a2802;
}

.callout-danger {
    border-left-color: #ab0f0e;
    background-color: #fcdbdb;
    color: #4d0706;
}

blockquote {
    background-color: #e9e9e9;
    color: black; 
    padding: 0.3rem;    
}

hr {
    page-break-after: always;
    clear: both;
    visibility: hidden;
}

</style>
</head>
<body>

"@

$htmlEnd = @"
</body>
</html>
"@

# get password from Hudu
New-HuduAPIKey $HuduAPIKey
New-HuduBaseURL $HuduBaseDomain

# grab the companies
$companies = Get-HuduCompanies

# grab the folders
$folders = Get-HuduFolders

# grab all the articles
$articles = Get-HuduArticles

if ($singleFile) {
    # create the output file with the html header
    $saveFile = "$outputFolder\printme.html"
    New-Item -path $savefile -force
    add-content -Value $htmlHeader -Path $saveFile

    # loop through all the articles and add them to saveFile
    # start with the companies
    foreach ($company in $companies) {
        # add title page for each company
        # we add a page even if there isn't information
        # this is for error correction
        $compDiv = "<h1 id=`"company`">$($company.name)</h1><hr>"
        add-content -Value $compDiv -path $saveFile

        # grab the articles that aren't in a folder for this company
        $rootArticles = $articles | Where-Object {$_.company_id -eq $company.id -and $_.folder_id -eq $null}

        # only add the header and articles if $rootArticles isn't null
        if ($null -ne $rootArticles) {
            $rootDiv = "<h1 id=`"folder`">Articles not in a Folder</h1><hr>"
            Add-Content -Value $rootDiv -Path $saveFile

            # loop through those articles and add them to the document
            foreach ($article in $rootArticles) {
                $content = $null

                $content = "<h1>$($article.name)</h1>" + $article.content + "<hr>"
                # add the article content to the document
                Add-Content -Value $content -path $saveFile
            }
        }

        $article = $null

        # sort out all the folders that belong in this company
        $companyFolders = $folders | Where-Object {$_.company_id -eq $company.id}

        # loop through the folders for this company
        foreach ($folder in $companyFolders) {
            # find out if there are any articles in the current folder
            $folderArticles = $articles | Where-object {$_.folder_id -eq $folder.id}

            # only add the folder page if there is something in it
            # yes, this is different than the companies. I feel that this addition cleans up the output
            # sometimes, folders are added in all companies, even if they are never needed
            if ($null -ne $folderArticles) {
                # add title page for each folder
                $folderDiv = "<h1 id=`"folder`">$($folder.name)</h1><hr>"
                Add-Content -Value $folderDiv -Path $saveFile
            }

            foreach ($article in $folderArticles) {
                # set variable to null to make sure we don't have issues
                $content = $null

                # set the article up to print out
                # ends with <hr> to create a page divider
                $content = "<h1>$($article.name)</h1>" + $article.content + "<hr>"
                # add the article content to the document
                Add-Content -Value $content -path $saveFile

            }
        }

    }
    # Add the articles that are in the global kb
    # Add a header
    $globalDiv = "<h1 id=`"company`">Global Articles</h1><hr>"
    Add-Content -Value $globalDiv -Path $saveFile

    # find the folders that aren't tied to a company
    $globalFolders = $folders | Where-Object {$_.company_id -eq $null}
    # find the articles that aren't tied to a company
    $globalArticles = $articles | Where-Object {$_.company_id -eq $null}

    foreach ($folder in $globalFolders) {
        $folderArticles = $globalArticles | Where-object {$_.folder_id -eq $folder.id}

        # add a title page for the folder if the folder has articles in it
        if ($null -ne $folderArticles) {
            # add title page for each folder
            $folderDiv = "<h1 id=`"folder`">$($folder.name)</h1><hr>"
            Add-Content -Value $folderDiv -Path $saveFile
        }

        foreach ($article in $folderArticles) {
            # set variable to null to make sure we don't have issues
            $content = $null

            # set the article up to print out
            # ends with <hr> to create a page divider
            $content = "<h1>$($article.name)</h1>" + $article.content + "<hr>"
            # add the article content to the document
            Add-Content -Value $content -path $saveFile
        }
    }


    # close out the html file
    Add-Content -Value $htmlEnd -Path $saveFile

}else {
    foreach ($article in $articles) {
        $content = $null
        $saveName = $null
        $company = $null
        $folder = $null
    
        $company = $companies | Where-Object {$_.id -eq $article.company_id} | Select-Object -ExpandProperty name
    
        if ($null -eq $company) {
            $company = "Global"
        }
    
        $folder = $folders | Where-Object {$_.id -eq $article.folder_id} | Select-Object -ExpandProperty name
    
        $saveName = "$($outputFolder)\$company\$folder\$($article.name).html"
        $content = $htmlHeader + "<h1 id=`"title`">$($article.name)</h1>" + $article.content + $htmlEnd
    
        $null = $content | Out-File (New-Item -path $saveName -force)
    
    }
}
